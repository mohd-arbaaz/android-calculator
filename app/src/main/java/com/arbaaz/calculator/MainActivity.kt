package com.arbaaz.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.arbaaz.calculator.databinding.ActivityMainBinding
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {
    private val postfixEvaluator : PostfixEvaluation = PostfixEvaluation()
    private val infixToPostfix : InfixToPostfix = InfixToPostfix()
    private lateinit var binding: ActivityMainBinding
    private var expression: String = ""


    private var lastNumeric: Boolean = false
    private var isDotPresent: Boolean = false
    private var previousOperator = ""
    private var lastDot : Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    fun onDigit(view: View) {
        val digit : String = (view as Button).text.toString()
        if(expression == "") {
            expression = digit
        } else {
            expression += if (lastNumeric || lastDot) "$digit" else " $digit"
        }
        setExpression()
        lastNumeric = true
        lastDot = false
    }

    fun onOperator(view: View) {
        val operator : String = (view as Button).text.toString()
        checkLastDot()
        if(expression == "") {
            Toast.makeText(this, "Insert a number before tapping an operator", Toast.LENGTH_SHORT).show()
        } else if (lastNumeric){
            expression += " $operator"
            lastNumeric = false
        } else if (!lastNumeric) {
            expression = expression.dropLast(1);
            expression += operator
        }
        setExpression()
    }

    fun onDot(view: View) {
        if(lastNumeric && !isDotPresent) {
            expression += "."
            lastNumeric = false
            isDotPresent = true
            lastDot = true
            setExpression()
        }
    }

    fun onClear(view: View) {
        loadDefaultState()
    }

    fun onEquals(view: View) {
        checkLastDot()
        if(lastNumeric) {
            expression = expression.replace('x', '*', true)
            val result : Double = postfixEvaluator.evaluate(infixToPostfix.processExpression(expression))
            binding.tvResult.text = result.toString()
            resetInput()
        } else {
            Toast.makeText(this, "Invalid Operation : The calculation is invalid or empty", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setExpression() {
        binding.tvInput.text = expression
    }

    private fun checkLastDot() {
        if(lastDot) {
            expression += "0"
            lastNumeric = true
            lastDot = false
        }
    }

    private fun resetInput() {
        lastNumeric = false
        lastDot = false
        isDotPresent = false
        expression = ""
        setExpression()
    }

    private fun loadDefaultState() {
        resetInput()
        previousOperator = ""
        binding.tvResult.text = ""
    }
}
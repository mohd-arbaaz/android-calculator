package com.arbaaz.calculator

import java.util.Stack

class PostfixEvaluation() {
    public fun evaluate(postfixExpression: String): Double {
        val contents = postfixExpression.split(" ").toTypedArray()
        val stack = Stack<Double>()
        for (str in contents) {
            if (str.matches("[0-9]+".toRegex())) {
                stack.push(str.toDouble())
            } else {
                val num2 = stack.pop()
                val num1 = stack.pop()
                var answer = 0.0
                when (str) {
                    "+" -> answer = num1 + num2
                    "-" -> answer = num1 - num2
                    "*" -> answer = num1 * num2
                    "/" -> answer = num1 / num2
                }
                stack.push(answer)
            }
        }
        return stack.pop()
    }
}
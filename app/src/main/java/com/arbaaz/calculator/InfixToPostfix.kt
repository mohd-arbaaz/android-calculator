package com.arbaaz.calculator

import java.util.*

class InfixToPostfix() {

    public fun processExpression(infixExpression: String): String {
        val contents = infixExpression.split(" ").toTypedArray()
        val operatorStack = Stack<String>()
        var postfixExpression = ""
        for (str in contents) {
            if (str.matches("[\\+\\-\\*\\/]".toRegex())) {
                while (!operatorStack.empty() && getPrecedence(str) <= getPrecedence(operatorStack.peek())) {
                    postfixExpression += " " + operatorStack.pop()
                }
                operatorStack.push(str)
            } else if (str.matches("\\(".toRegex())) {
                operatorStack.push(str)
            } else if (str.matches("\\)".toRegex())) {
                while (!operatorStack.empty() && operatorStack.peek() != "(") {
                    postfixExpression += " " + operatorStack.pop()
                }
                operatorStack.pop()
            }   else {
                if (postfixExpression == "") {
                    postfixExpression = str
                } else {
                    postfixExpression += " $str"
                }
            }
        }
        while (!operatorStack.empty()) {
            postfixExpression += " " + operatorStack.pop()
        }
        return postfixExpression
    }

    private fun getPrecedence(str: String): Int {
        when (str) {
            "+", "-" -> return 1
            "*", "/" -> return 2
            "^" -> return 3
        }
        return -1
    }
}
